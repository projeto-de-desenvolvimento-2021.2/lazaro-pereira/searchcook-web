import React, { useEffect, useState } from "react";
import { Col, Row, Container, Image } from "react-bootstrap";
import { useParams } from "react-router-dom";
import api from "../../api";
import "./recipeDetail.css";

export default function RecipeDetail() {
  const { id } = useParams();

  const [recipe, setRecipes] = useState([]);
  const [ingredient, setIngredient] = useState([]);

  const getRecipe = async (id) => {
    await api.get(`/recipe/${id}`).then((res) => {
      setRecipes(res.data.response);
      setIngredient(res.data.response.ingredients);
    });
  };
  // const quantities = recipe.quantities;
  // const listQuantities = quantities.map((item) => (
  //   <li className="recipe-list">{item}</li>
  // ));

  // const measures = recipe.measures;
  // const listMeasures = measures.map((item) => (
  //   <li className="recipe-list">{item}</li>
  // ));

  const listIngredients = ingredient.map((item) => (
    <li className="recipe-list">{item}</li>
  ));

  useEffect(() => {
    getRecipe(id);
  }, []);

  return (
 
        <Container>
          <Row className="mt-5">
            <Col md={6}>
              <Image src={recipe.photo} fluid rounded style={{ objectFit: "cover", height: "200px", width: "300px" }} />
            </Col>
            <Col md={6}>
              <h1>{recipe.name}</h1>
              <h3>{recipe.category}</h3>
            </Col>
          </Row>
          <Row className="mt-5">
            {/* <h1>Ingredientes</h1>
        <Col md={1}>
          <h5>Qtd</h5>
          <ul>{listQuantities}</ul>
        </Col>
        <Col md={2}>
          <h5>Medida</h5>
          <ul>{listMeasures}</ul>
        </Col> */}
            <Col md={5}>
              <h5>Ingredientes</h5>
              <ul>{listIngredients}</ul>
              {/* <ul>{listIngredients}</ul> */}
            </Col>
          </Row>

          <Row>
            <Col md={6}>
              <h5>Preparo</h5>
              <p>{recipe.prepare}</p>
            </Col>
          </Row>
        </Container>
      )
  
}
