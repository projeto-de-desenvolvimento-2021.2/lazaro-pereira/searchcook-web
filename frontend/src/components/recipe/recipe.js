import React from "react";
import { Col, Card, Button } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrash, faEdit } from "@fortawesome/free-solid-svg-icons";
import api from "../../api";
import "./recipe.css";

import { Link } from "react-router-dom";
export default function Recipe(recipe) {
  async function handleRemove(id) {
    try {
      await api.delete(`/recipe/${id}`);
      alert("Sucesso, deletado");
      window.location.reload();
    } catch (e) {
      console.log(e);
      alert("Erro ao deletar");
    }
  }

  async function handleEdit() {
    window.location.href = "/formrecipeedit";
  }

  return (
    <Col md={4} className="">
      <Card
        key={recipe.id}
        className="border-0 mt-5 bg-color-recipe recipe-height"
      >
        <Link to={`/recipe/${recipe.id}`} className="link-recipe">
          <Card.Img
            variant="top"
            className="recipe-photo"
            style={{ objectFit: "cover", height: "200px", width: "100%" }}
            src={recipe.photo}
          />
          <Card.Body className="text-center">
            <Card.Title>{recipe.name}</Card.Title>
            <Card.Text>{recipe.category}</Card.Text>
          </Card.Body>
        </Link>
        <Button
          className="mb-3"
          color="secondary"
          onClick={() => handleRemove(recipe.id)}
        >
          <FontAwesomeIcon icon={faTrash} />
        </Button>
        <Button
          className="mb-3"
          color="secondary"
          onClick={() => {window.location.href=`/recipe/edit/${recipe.id}`}}
        >
          <FontAwesomeIcon icon={faEdit} />
        </Button>
      </Card>
    </Col>
  );
}
