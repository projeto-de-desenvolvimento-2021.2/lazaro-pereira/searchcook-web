import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import {
  Row,
  Col,
  Button,
  Container,
  FormGroup,
  Form,
  FloatingLabel,
} from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus, faTrash } from "@fortawesome/free-solid-svg-icons";
import "./FormRecipe.css";
import api from "../../api";

export default function FormRecipeEdit() {
  const [name, setName] = useState("");
  const [category, setCategory] = useState("");
  const [prepare, setPrepare] = useState("");
  const [photo, setPhoto] = useState("");
  const [ingredient, setIngredient] = useState([]);
  const [quantity, setQuantity] = useState([]);
  const [measure, setMeasure] = useState([]);

  const { id } = useParams();

  async function handleRegister() {
    const userIdSession = localStorage.getItem("id");

    const data = {
      name: name,
      category: category,
      ingredients: ingredient,
      quantities: quantity,
      measures: measure,
      prepare: prepare,
      photo: photo,
      owner: userIdSession,
    };

    try {
      await api.put(`/recipe/${id}`, data);
      alert("Receita editada com sucesso");
      window.location.href = "/";
    } catch (e) {
      console.log(e);
      alert("Erro ao cadastrar" + data.name);
    }
  }

  const getRecipe = async (id) => {
    await api.get(`/recipe/${id}`).then((res) => {
      setName(res.data.response.name);
      setCategory(res.data.response.category);
      setIngredient(res.data.response.ingredients);
      setPrepare(res.data.response.prepare);
      setPhoto(res.data.response.photo);
    });
  };

  const addInputButtonIngredient = () => {
    setIngredient([...ingredient, ""]);

    console.log(ingredient);
  };

  const addInputButtonQuantity = () => {
    setQuantity([...quantity, ""]);
  };

  const addInputButtonMeasure = () => {
    setMeasure([...measure, ""]);
  };

  const addInputFields = (e) => {
    e.preventDefault();
    addInputButtonIngredient();
    addInputButtonQuantity();
    addInputButtonMeasure();
  };

  const removeInputFields = (position) => {
    setQuantity([...quantity.filter((_, index) => index !== position)]);
    setMeasure([...measure.filter((_, index) => index !== position)]);
    setIngredient([...ingredient.filter((_, index) => index !== position)]);
  };

  const handleChangeIngredient = (e, index) => {
    ingredient[index] = e.target.value;
    setIngredient([...ingredient]);
  };

  const handleChangeQuantity = (e, index) => {
    quantity[index] = e.target.value;
    setQuantity([...quantity]);
  };

  const handleChangeMeasure = (e, index) => {
    measure[index] = e.target.value;
    setMeasure([...measure]);
  };


  useEffect(() => {
    getRecipe(id);
  }, []);

  return (
    <Container>
      <Form
        className="card card-body mt-4 bg-transparent border-0"
      >
        <Row>
          <Col md={5}>
            <FormGroup>
              <FloatingLabel label="Receita" className="mb-3">
                <Form.Control
                  type="text"
                  placeholder="Nome da receita"
                  value={name}
                  className="form-control"
                  id="recipeName"
                  onChange={(e) => setName(e.target.value)}
                />
              </FloatingLabel>
            </FormGroup>
          </Col>
        </Row>

        <Row className="mt-3">
          <Col md={3}>
            <FormGroup>
              <Form.Select
                aria-label="Category"
                value={category}
                onChange={(e) => setCategory(e.target.value)}
              >
                <option value="default" disabled>
                  Selecione a categoria
                </option>
                <option value="Carnes">Carnes</option>
                <option value="Massas">Massas</option>
                <option value="Sobremesas">Sobremesas</option>
                <option value="Lanches">Lanches</option>
                <option value="Saudavel">Saudavel</option>
                <option value="Tortas">Tortas</option>
                <option value="Frutosdomar">Frutos do mar</option>
                <option value="Bebidas">Bebidas</option>
              </Form.Select>
            </FormGroup>
          </Col>
        </Row>

        <label className="form-label mt-3">Ingredientes</label>

        <Row>
          <Col>
            <Button
              className="mb-3 button-add"
              color="secondary"
              onClick={addInputFields}
            >
              Adicionar ingrediente <FontAwesomeIcon icon={faPlus} />
            </Button>
          </Col>
        </Row>

        <Row className="mb-3">
          {/* {quantity.map((quantity, index) => (
            <Col md={1} key={`quantity${index}`}>
              <Form.Group controlId="formGridquantity">
                <FloatingLabel label={`Qtd ${index + 1}`} className="mb-3">
                  <Form.Control
                    type="number"
                    min="1"
                    name="quantity"
                    id={`Quantity${index + 1}`}
                    value={quantity}
                    placeholder="Quantaidade"
                    onChange={(e) => handleChangeQuantity(e, index)}
                  />
                </FloatingLabel>
              </Form.Group>
            </Col>
          ))}

          {measure.map((measure, index) => (
            <Col md={3}>
              <FormGroup>
                <FloatingLabel label={`Medida ${index + 1}`} className="mb-3">
                  <Form.Select
                    aria-label="Measure"
                    id={`Measure${index + 1}`}
                    value={measure}
                    onChange={(e) => handleChangeMeasure(e, index)}
                  >
                    <option disabled>Selecione a medida</option>
                    <option>Unidade(s)</option>
                    <option>Grama(s)</option>
                    <option>Quilo(s)</option>
                    <option>ml(s)</option>
                    <option>Litro(s)</option>
                    <option>Taça(s)</option>
                    <option>Xícara(s) de chá</option>
                    <option>Colher(es) de chá</option>
                    <option>Colher(es) de Sopa</option>
                    <option>Colher(es) de café</option>
                    <option>Caixa/Lata</option>
                  </Form.Select>
                </FloatingLabel>
              </FormGroup>
            </Col>
          ))} */}

          {ingredient.map((ingredient, index) => (
            <>
              <Col md={6} key={`ingredient${index}`}>
                <Form.Group controlId="formGridingredient">
                  <FloatingLabel
                    label={`Ingrediente ${index + 1}`}
                    className="mb-3"
                  >
                    <Form.Control
                      type="text"
                      name="ingredient"
                      id={`Ingredient${index + 1}`}
                      value={ingredient}
                      placeholder="Ingredientes"
                      onChange={(e) => handleChangeIngredient(e, index)}
                    />
                  </FloatingLabel>
                </Form.Group>
              </Col>

              <Col md={1}>
                <Button
                  className="button-remove"
                  onClick={() => {
                    removeInputFields(index);
                  }}
                >
                  <FontAwesomeIcon icon={faTrash} />
                </Button>
              </Col>
            </>
          ))}
        </Row>

        <Row className="mt-4">
          <Col md={8}>
            <FloatingLabel label="Modo de preparo" className="mb-3">
              <Form.Control
                as="textarea"
                placeholder="Modo de preparo"
                value={prepare}
                rows={5}
                id="prepare"
                onChange={(e) => setPrepare(e.target.value)}
              ></Form.Control>
            </FloatingLabel>
          </Col>
        </Row>

        <Row className="mt-4">
          <Col md={12}>
            <Row>
              <Col md={8}>
                <input
                  type="text"
                  value={photo}
                  className="form-control"
                  id="photo"
                  onChange={(e) => setPhoto(e.target.value)}
                />
              </Col>
            </Row>
          </Col>
        </Row>
        {/* <Row className="mt-4">
          <Col md={12}>
            <Row>
              <Col md={8}>
                <input
                  type="file"
                  className="form-control"
                  id="photo"
                  onChange={(e) => setPhoto(e.target.value)}
                />
              </Col>
            </Row>
          </Col>
        </Row> */}

      </Form>
        <Row>
          <Col md={4} className="d-grid gap-2 mt-4">
            <Button type="submit" className="btn btn-block button-registry" onClick={handleRegister}>
              Confirmar edição
            </Button>
          </Col>
        </Row>
    </Container>
  );
}
