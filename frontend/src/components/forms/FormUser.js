import React, { useState } from "react";
import { Container, Form, Row, Col } from "react-bootstrap";
import api from "../../api";
import "./FormUser.css";

export default function FormUser() {
  const [name, setName] = useState("");
  const [birthDate, setBirthDate] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [photo, setPhoto] = useState("");

  async function handleRegister() {
    const data = {
      name: name,
      birthDate: birthDate,
      email: email,
      password: password,
      photo: photo,
    };
    console.log(data);
    try {
      await api.post("/user", data);
      alert("Sucesso");
    } catch (e) {
      console.log(e);
      alert("Erro ao cadastrar");
    }
  }

  return (
    <Container>
      <Form
        onSubmit={handleRegister}
        className="card card-body mt-4 bg-transparent border-0"
      >
        <Row>
          <Col md={6}>
            <label className="form-label">Nome</label>
            <input
              type="text"
              className="form-control"
              id="name"
              onChange={(e) => setName(e.target.value)}
            />
          </Col>

          <Col md={6}>
            <label className="form-label">Data de nascimento</label>
            <input
              type="date"
              className="form-control"
              id="birthDate"
              onChange={(e) => setBirthDate(e.target.value)}
            />
          </Col>
        </Row>

        <Row className="mt-4">
          <Col md={6}>
            <label className="form-label">E-mail</label>
            <input
              type="email"
              className="form-control"
              id="email"
              onChange={(e) => setEmail(e.target.value)}
            />
          </Col>

          <Col md={6}>
            <label className="form-label">Senha</label>
            <input
              type="password"
              className="form-control"
              id="password"
              onChange={(e) => setPassword(e.target.value)}
            />
          </Col>
        </Row>

        <Row className="mt-4">
          <Col md={6}>
            <label className="form-label">Confirme seu E-mail</label>
            <input
              type="email"
              className="form-control"
              id="emailConfirmation"
            />
          </Col>

          <Col md={6}>
            <label className="form-label">Confirme sua senha</label>
            <input
              type="password"
              className="form-control"
              id="passwordConfirmation"
            />
          </Col>
        </Row>

        <Row className="mt-4">
          <Col md={12}>
            <label className="form-label">Selecione a foto</label>
            <input
              type="text"
              className="form-control"
              id="photoUser"
              onChange={(e) => setPhoto(e.target.value)}
            />
          </Col>
        </Row>
        {/* <Row className="mt-4">
          <Col md={12}>
            <label className="form-label">Selecione a foto</label>
            <input
              type="file"
              className="form-control"
              id="photoUser"
              //defaultValue={photo}
              onChange={e => setPhoto(e.target.value)}
              //{...register("photo", { required: true, maxLength: 5 })}
            />
          </Col>
        </Row> */}

        <Row>
          <div className="d-grid gap-2 mt-4">
            <button type="submit" className="btn btn-block button-registry">
              Cadastrar
            </button>
          </div>
        </Row>
      </Form>
    </Container>
  );
}
