import React from "react";
import {
  Navbar,
  Nav,
  NavDropdown,
  Container,
  Image,
  Button,
  Form,
  FormControl
} from "react-bootstrap";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSignInAlt, faSignOutAlt } from "@fortawesome/free-solid-svg-icons";
import "./Header.css";
import Logo from "../../assets/images/logo.png";

export default function Header() {
  const userName = localStorage.getItem("name");
  let headerUser;

  const logout = () => {
    localStorage.clear();
    window.location.href = "/";
  };

  if (userName !== null) {
    headerUser = (
      <Nav className="ms-auto0">
        <NavDropdown title={userName} id="basic-nav-dropdown">
          <NavDropdown.Item href="#action/3.1">Minha conta</NavDropdown.Item>
          <NavDropdown.Item href="#action/3.2">
            Minhas receitas
          </NavDropdown.Item>
          <NavDropdown.Item href="/formrecipe">
            Cadastrar receita
          </NavDropdown.Item>
          <NavDropdown.Item href="#action/3.4">
            Receitas favoritas
          </NavDropdown.Item>
          <NavDropdown.Item onClick={() => logout()} href="/">
            Sair
          </NavDropdown.Item>
        </NavDropdown>
        <Button onClick={() => logout()}>
          <FontAwesomeIcon icon={faSignOutAlt} className="ml-2" />
        </Button>
      </Nav>
    );
  }

  if (userName === null) {
    headerUser = (
      <Nav className="ms-auto0">
        <Link to="/formuser" className="link-header">
          Cadastrar
        </Link>

        <Link to="/formlogin" className="nav-link">
          Entrar
          <FontAwesomeIcon icon={faSignInAlt} className="ml-2" />
        </Link>
      </Nav>
    );
  }

  return (
    <>
      <Navbar className="navColor" expand="lg">
        <Container>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse
            id="basic-navbar-nav"
            className="justify-content-between"
          >
            <Nav>
              <NavDropdown
                title="Categorias"
                id="basic-nav-dropdown"
                className="border-0"
              >
                <NavDropdown.Item href="#action/3.1">Carnes</NavDropdown.Item>
                <NavDropdown.Item href="#action/3.2">Massas</NavDropdown.Item>
                <NavDropdown.Item href="#action/3.3">
                  Sobremesas
                </NavDropdown.Item>
                <NavDropdown.Item href="#action/3.4">Lanches</NavDropdown.Item>
                <NavDropdown.Item href="#action/3.4">Saudavel</NavDropdown.Item>
                <NavDropdown.Item href="#action/3.4">Tortas</NavDropdown.Item>
                <NavDropdown.Item href="#action/3.4">
                  Frutos do mar
                </NavDropdown.Item>
                <NavDropdown.Item href="#action/3.4">Bebidas</NavDropdown.Item>
              </NavDropdown>
            </Nav>
            <Nav>
              <Link to="/">
                <Image src={Logo} className="image-custom"></Image>
              </Link>
            </Nav>
            {headerUser}
          </Navbar.Collapse>
        </Container>
      </Navbar>

      {/* <Navbar>
        <Container>
          <Form className="d-flex">
            <FormControl
              type="search"
              placeholder="Search"
              className="me-2"
              aria-label="Search"
            />
            <Button variant="outline-success">Search</Button>
          </Form>
        </Container>
      </Navbar> */}
    </>
  );
}
